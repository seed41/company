terraform{
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "us-east-2"
}

resource "aws_s3_bucket" "manideep26212" {
  bucket = "manideep26212"
  versioning {
    enabled = true
  }
}

resource "aws_iam_role" "glue_execution_role_26212" {
  name               = "glue_execution_role_26212"
  assume_role_policy = file("assumrole.json")
}

resource "aws_iam_policy" "glue_execution_policy_26212" {
  name        = "glue_execution_policy_26212"
  description = "glue execution policy"
  policy      = file("glue_policy.json")
}

resource "aws_iam_role_policy_attachment" "glue_policy_attachment" {
  role       = aws_iam_role.glue_execution_role_26212.name
  policy_arn = aws_iam_policy.glue_execution_policy_26212.arn
}

resource "aws_glue_catalog_database" "manideep" {
  name = "manideep"
}

resource "aws_glue_crawler" "crawler-212" {
  database_name = aws_glue_catalog_database.manideep.name
  name          = "crawler-212"
  role          = aws_iam_role.glue_execution_role_26212.arn

  s3_target {
    path = "s3://${aws_s3_bucket.manideep26212.bucket}"
  }
}
resource "aws_glue_job" "job-212" {
  name     = "job-212"
  role_arn = aws_iam_role.glue_execution_role_26212.arn

  command {
    script_location = "s3://manideep26212/glue.py"
  }
}


