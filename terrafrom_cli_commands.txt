
E:\aws>md tfdemoex2

E:\aws>cd tfdemoex2

E:\aws\tfdemoex2>set pat=%path%;E:aws

E:\aws\tfdemoex2>terraform
Usage: terraform [global options] <subcommand> [args]

The available commands for execution are listed below.
The primary workflow commands are given first, followed by
less common or more advanced commands.

Main commands:
  init          Prepare your working directory for other commands
  validate      Check whether the configuration is valid
  plan          Show changes required by the current configuration
  apply         Create or update infrastructure
  destroy       Destroy previously-created infrastructure

All other commands:
  console       Try Terraform expressions at an interactive command prompt
  fmt           Reformat your configuration in the standard style
  force-unlock  Release a stuck lock on the current workspace
  get           Install or upgrade remote Terraform modules
  graph         Generate a Graphviz graph of the steps in an operation
  import        Associate existing infrastructure with a Terraform resource
  login         Obtain and save credentials for a remote host
  logout        Remove locally-stored credentials for a remote host
  output        Show output values from your root module
  providers     Show the providers required for this configuration
  refresh       Update the state to match remote systems
  show          Show the current state or a saved plan
  state         Advanced state management
  taint         Mark a resource instance as not fully functional
  test          Experimental support for module integration testing
  untaint       Remove the 'tainted' state from a resource instance
  version       Show the current Terraform version
  workspace     Workspace management

Global options (use these before the subcommand, if any):
  -chdir=DIR    Switch to a different working directory before executing the
                given subcommand.
  -help         Show this help output, or the help for a specified subcommand.
  -version      An alias for the "version" subcommand.

E:\aws\tfdemoex2>notepad main.tf

E:\aws\tfdemoex2>dir
 Volume in drive E is New Volume
 Volume Serial Number is FA33-1D3E

 Directory of E:\aws\tfdemoex2

08/13/2021  09:33 AM    <DIR>          .
08/13/2021  09:33 AM    <DIR>          ..
08/13/2021  09:34 AM               193 main.tf
               1 File(s)            193 bytes
               2 Dir(s)  796,963,471,360 bytes free

E:\aws\tfdemoex2>terraform init

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/aws versions matching "~> 3.0"...
- Installing hashicorp/aws v3.54.0...
- Installed hashicorp/aws v3.54.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

E:\aws\tfdemoex2>terraform apply

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

E:\aws\tfdemoex2>notepad glue.py

E:\aws\tfdemoex2>terraform init

Initializing the backend...

Initializing provider plugins...
- Reusing previous version of hashicorp/aws from the dependency lock file
- Using previously-installed hashicorp/aws v3.54.0

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

E:\aws\tfdemoex2>notepad assumrole.json

E:\aws\tfdemoex2>notepad glue_policy.json


E:\aws\tfdemoex2>terraform apply
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the last "terraform apply":

  # aws_s3_bucket.manideep26212 has been changed
  ~ resource "aws_s3_bucket" "manideep26212" {
        id                          = "manideep26212"
      + tags                        = {}
        # (10 unchanged attributes hidden)

        # (1 unchanged block hidden)
    }
  # aws_glue_catalog_database.manideep has been changed
  ~ resource "aws_glue_catalog_database" "manideep" {
        id         = "623734501964:manideep"
        name       = "manideep"
      + parameters = {}
        # (2 unchanged attributes hidden)
    }

Unless you have made equivalent changes to your configuration, or ignored the relevant attributes using ignore_changes, the following plan may include actions to undo or respond to these changes.

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_glue_crawler.crawler-212 will be created
  + resource "aws_glue_crawler" "crawler-212" {
      + arn           = (known after apply)
      + database_name = "manideep"
      + id            = (known after apply)
      + name          = "crawler-212"
      + role          = (known after apply)
      + tags_all      = (known after apply)

      + s3_target {
          + path = "s3://manideep26212"
        }
    }

  # aws_iam_policy.glue_execution_policy_26212 will be created
  + resource "aws_iam_policy" "glue_execution_policy_26212" {
      + arn         = (known after apply)
      + description = "glue execution policy"
      + id          = (known after apply)
      + name        = "glue_execution_policy_26212"
      + path        = "/"
      + policy      = jsonencode(
            {
              + Statement = [
                  + {
                      + Action   = [
                          + "glue:*",
                          + "CloudWatch:*",
                          + "S3:*",
                        ]
                      + Effect   = "Allow"
                      + Resource = "*"
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + policy_id   = (known after apply)
      + tags_all    = (known after apply)
    }

  # aws_iam_role.glue_execution_role_26212 will be created
  + resource "aws_iam_role" "glue_execution_role_26212" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + Service = "glue.amazonaws.com"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "glue_execution_role_26212"
      + path                  = "/"
      + tags_all              = (known after apply)
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # aws_iam_role_policy_attachment.glue_policy_attachment will be created
  + resource "aws_iam_role_policy_attachment" "glue_policy_attachment" {
      + id         = (known after apply)
      + policy_arn = (known after apply)
      + role       = "glue_execution_role_26212"
    }

Plan: 4 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_iam_policy.glue_execution_policy_26212: Creating...
aws_iam_role.glue_execution_role_26212: Creating...
aws_iam_policy.glue_execution_policy_26212: Creation complete after 3s [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_iam_role.glue_execution_role_26212: Creation complete after 4s [id=glue_execution_role_26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Creating...
aws_glue_crawler.crawler-212: Creating...
aws_iam_role_policy_attachment.glue_policy_attachment: Creation complete after 3s [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_crawler.crawler-212: Creation complete after 8s [id=crawler-212]

Apply complete! Resources: 4 added, 0 changed, 0 destroyed.

E:\aws\tfdemoex2>aws glue start-crawler --name crawler-212

E:\aws\tfdemoex2>terraform apply
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_iam_policy.glue_execution_policy_26212: Refreshing state... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_iam_role.glue_execution_role_26212: Refreshing state... [id=glue_execution_role_26212]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Refreshing state... [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_crawler.crawler-212: Refreshing state... [id=crawler-212]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the last "terraform apply":

  # aws_glue_crawler.crawler-212 has been changed
  ~ resource "aws_glue_crawler" "crawler-212" {
      + classifiers   = []
        id            = "crawler-212"
        name          = "crawler-212"
      + tags          = {}
        # (4 unchanged attributes hidden)



      ~ s3_target {
          + exclusions  = []
            # (2 unchanged attributes hidden)
        }

        # (3 unchanged blocks hidden)
    }
  # aws_iam_policy.glue_execution_policy_26212 has been changed
  ~ resource "aws_iam_policy" "glue_execution_policy_26212" {
        id          = "arn:aws:iam::623734501964:policy/glue_execution_policy_26212"
        name        = "glue_execution_policy_26212"
      + tags        = {}
        # (6 unchanged attributes hidden)
    }
  # aws_iam_role.glue_execution_role_26212 has been changed
  ~ resource "aws_iam_role" "glue_execution_role_26212" {
        id                    = "glue_execution_role_26212"
      ~ managed_policy_arns   = [
          + "arn:aws:iam::623734501964:policy/glue_execution_policy_26212",
        ]
        name                  = "glue_execution_role_26212"
      + tags                  = {}
        # (8 unchanged attributes hidden)

        # (1 unchanged block hidden)
    }

Unless you have made equivalent changes to your configuration, or ignored the relevant attributes using ignore_changes, the following plan may include actions to undo or respond to these changes.

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  ~ update in-place

Terraform will perform the following actions:

  # aws_iam_policy.glue_execution_policy_26212 will be updated in-place
  ~ resource "aws_iam_policy" "glue_execution_policy_26212" {
        id          = "arn:aws:iam::623734501964:policy/glue_execution_policy_26212"
        name        = "glue_execution_policy_26212"
      ~ policy      = jsonencode(
          ~ {
              ~ Statement = [
                  ~ {
                      ~ Action   = [
                          + "s3:*",
                            "glue:*",
                          - "CloudWatch:*",
                          - "S3:*",
                          + "autoscaling:Describe*",
                          + "cloudwatch:*",
                          + "logs:*",
                          + "iam:GetPolicy",
                          + "iam:GetPolicyVersion",
                          + "iam:GetRole",
                        ]
                        # (2 unchanged elements hidden)
                    },
                ]
                # (1 unchanged element hidden)
            }
        )
        tags        = {}
        # (5 unchanged attributes hidden)
    }

Plan: 0 to add, 1 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_iam_policy.glue_execution_policy_26212: Modifying... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_iam_policy.glue_execution_policy_26212: Modifications complete after 4s [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]

Apply complete! Resources: 0 added, 1 changed, 0 destroyed.

E:\aws\tfdemoex2>aws glue start-crawler --name crawler-212

E:\aws\tfdemoex2>terraform apply
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_iam_policy.glue_execution_policy_26212: Refreshing state... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_iam_role.glue_execution_role_26212: Refreshing state... [id=glue_execution_role_26212]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Refreshing state... [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_crawler.crawler-212: Refreshing state... [id=crawler-212]

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

E:\aws\tfdemoex2>aws glue start-crawler --name crawler-212

E:\aws\tfdemoex2>terraform apply
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_iam_policy.glue_execution_policy_26212: Refreshing state... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_iam_role.glue_execution_role_26212: Refreshing state... [id=glue_execution_role_26212]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Refreshing state... [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_crawler.crawler-212: Refreshing state... [id=crawler-212]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_glue_job.job-212 will be created
  + resource "aws_glue_job" "job-212" {
      + arn          = (known after apply)
      + glue_version = (known after apply)
      + id           = (known after apply)
      + max_capacity = (known after apply)
      + name         = "job-212"
      + role_arn     = "arn:aws:iam::623734501964:role/glue_execution_role_26212"
      + tags_all     = (known after apply)
      + timeout      = 2880

      + command {
          + name            = "glueetl"
          + python_version  = (known after apply)
          + script_location = "s3://manideep26212.bucket/glue.py"
        }

      + execution_property {
          + max_concurrent_runs = (known after apply)
        }

      + notification_property {
          + notify_delay_after = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_glue_job.job-212: Creating...
aws_glue_job.job-212: Creation complete after 4s [id=job-212]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

E:\aws\tfdemoex2>terraform apply
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_iam_policy.glue_execution_policy_26212: Refreshing state... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_glue_job.job-212: Refreshing state... [id=job-212]
aws_iam_role.glue_execution_role_26212: Refreshing state... [id=glue_execution_role_26212]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Refreshing state... [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_crawler.crawler-212: Refreshing state... [id=crawler-212]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the last "terraform apply":

  # aws_glue_job.job-212 has been changed
  ~ resource "aws_glue_job" "job-212" {
      + connections               = []
      + default_arguments         = {}
        id                        = "job-212"
        name                      = "job-212"
      + non_overridable_arguments = {}
      + tags                      = {}
        # (7 unchanged attributes hidden)


        # (2 unchanged blocks hidden)
    }

Unless you have made equivalent changes to your configuration, or ignored the relevant attributes using ignore_changes, the following plan may include actions to undo or respond to these changes.

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # aws_glue_job.job-212 will be destroyed
  - resource "aws_glue_job" "job-212" {
      - arn                       = "arn:aws:glue:us-east-2:623734501964:job/job-212" -> null
      - connections               = [] -> null
      - default_arguments         = {} -> null
      - id                        = "job-212" -> null
      - max_capacity              = 10 -> null
      - max_retries               = 0 -> null
      - name                      = "job-212" -> null
      - non_overridable_arguments = {} -> null
      - number_of_workers         = 0 -> null
      - role_arn                  = "arn:aws:iam::623734501964:role/glue_execution_role_26212" -> null
      - tags                      = {} -> null
      - tags_all                  = {} -> null
      - timeout                   = 2880 -> null

      - command {
          - name            = "glueetl" -> null
          - python_version  = "2" -> null
          - script_location = "s3://manideep26212.bucket/glue.py" -> null
        }

      - execution_property {
          - max_concurrent_runs = 1 -> null
        }
    }

Plan: 0 to add, 0 to change, 1 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_glue_job.job-212: Destroying... [id=job-212]
aws_glue_job.job-212: Destruction complete after 1s

Apply complete! Resources: 0 added, 0 changed, 1 destroyed.

E:\aws\tfdemoex2>aws glue start-crawler --name crawler-212

E:\aws\tfdemoex2>terraform apply
aws_iam_policy.glue_execution_policy_26212: Refreshing state... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]
aws_iam_role.glue_execution_role_26212: Refreshing state... [id=glue_execution_role_26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Refreshing state... [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_crawler.crawler-212: Refreshing state... [id=crawler-212]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_glue_job.job-212 will be created
  + resource "aws_glue_job" "job-212" {
      + arn          = (known after apply)
      + glue_version = (known after apply)
      + id           = (known after apply)
      + max_capacity = (known after apply)
      + name         = "job-212"
      + role_arn     = "arn:aws:iam::623734501964:role/glue_execution_role_26212"
      + tags_all     = (known after apply)
      + timeout      = 2880

      + command {
          + name            = "glueetl"
          + python_version  = (known after apply)
          + script_location = "s3://manideep26212.bucket/glue.py"
        }

      + execution_property {
          + max_concurrent_runs = (known after apply)
        }

      + notification_property {
          + notify_delay_after = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_glue_job.job-212: Creating...
aws_glue_job.job-212: Creation complete after 4s [id=job-212]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

E:\aws\tfdemoex2>terraform apply
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_iam_policy.glue_execution_policy_26212: Refreshing state... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_iam_role.glue_execution_role_26212: Refreshing state... [id=glue_execution_role_26212]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Refreshing state... [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_job.job-212: Refreshing state... [id=job-212]
aws_glue_crawler.crawler-212: Refreshing state... [id=crawler-212]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the last "terraform apply":

  # aws_glue_job.job-212 has been deleted
  - resource "aws_glue_job" "job-212" {
      - arn               = "arn:aws:glue:us-east-2:623734501964:job/job-212" -> null
      - id                = "job-212" -> null
      - max_capacity      = 10 -> null
      - max_retries       = 0 -> null
      - name              = "job-212" -> null
      - number_of_workers = 0 -> null
      - role_arn          = "arn:aws:iam::623734501964:role/glue_execution_role_26212" -> null
      - tags_all          = {} -> null
      - timeout           = 2880 -> null

      - command {
          - name            = "glueetl" -> null
          - python_version  = "2" -> null
          - script_location = "s3://manideep26212.bucket/glue.py" -> null
        }

      - execution_property {
          - max_concurrent_runs = 1 -> null
        }
    }

Unless you have made equivalent changes to your configuration, or ignored the relevant attributes using ignore_changes, the following plan may include actions to undo or respond to these changes.

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_glue_job.job-212 will be created
  + resource "aws_glue_job" "job-212" {
      + arn          = (known after apply)
      + glue_version = (known after apply)
      + id           = (known after apply)
      + max_capacity = (known after apply)
      + name         = "job-212"
      + role_arn     = "arn:aws:iam::623734501964:role/glue_execution_role_26212"
      + tags_all     = (known after apply)
      + timeout      = 2880

      + command {
          + name            = "glueetl"
          + python_version  = (known after apply)
          + script_location = "s3://manideep26212.bucket/glue.py"
        }

      + execution_property {
          + max_concurrent_runs = (known after apply)
        }

      + notification_property {
          + notify_delay_after = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_glue_job.job-212: Creating...
aws_glue_job.job-212: Creation complete after 3s [id=job-212]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

E:\aws\tfdemoex2>terraform apply
aws_glue_catalog_database.manideep: Refreshing state... [id=623734501964:manideep]
aws_iam_policy.glue_execution_policy_26212: Refreshing state... [id=arn:aws:iam::623734501964:policy/glue_execution_policy_26212]
aws_iam_role.glue_execution_role_26212: Refreshing state... [id=glue_execution_role_26212]
aws_s3_bucket.manideep26212: Refreshing state... [id=manideep26212]
aws_iam_role_policy_attachment.glue_policy_attachment: Refreshing state... [id=glue_execution_role_26212-20210813044457660600000001]
aws_glue_job.job-212: Refreshing state... [id=job-212]
aws_glue_crawler.crawler-212: Refreshing state... [id=crawler-212]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the last "terraform apply":

  # aws_glue_job.job-212 has been changed
  ~ resource "aws_glue_job" "job-212" {
      + connections               = []
      + default_arguments         = {}
        id                        = "job-212"
        name                      = "job-212"
      + non_overridable_arguments = {}
      + tags                      = {}
        # (7 unchanged attributes hidden)


        # (2 unchanged blocks hidden)
    }

Unless you have made equivalent changes to your configuration, or ignored the relevant attributes using ignore_changes, the following plan may include actions to undo or respond to these changes.

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  ~ update in-place

Terraform will perform the following actions:

  # aws_glue_job.job-212 will be updated in-place
  ~ resource "aws_glue_job" "job-212" {
        id                        = "job-212"
        name                      = "job-212"
        tags                      = {}
        # (10 unchanged attributes hidden)

      ~ command {
            name            = "glueetl"
          ~ script_location = "s3://manideep26212.bucket/glue.py" -> "s3://manideep26212/glue.py"
            # (1 unchanged attribute hidden)
        }

        # (1 unchanged block hidden)
    }

Plan: 0 to add, 1 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_glue_job.job-212: Modifying... [id=job-212]
aws_glue_job.job-212: Modifications complete after 4s [id=job-212]

Apply complete! Resources: 0 added, 1 changed, 0 destroyed.